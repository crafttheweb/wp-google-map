<?php
namespace TheFold\Locations;

use TheFold\WordPress;

if(function_exists("register_field_group")) {
//if(!getEnv('DEV_SITE') && function_exists("register_field_group")) {
    
    $group = array (
        'id' => 'acf_locations',
        'title' => 'Locations',
        'fields' => array (
            array (
                'key' => 'field_539515ee77f88',
                'label' => 'Location',
                'name' => 'location',
                'type' => 'google_map',
                'center_lat' => get_setting('center_lat'),
                'center_lng' => get_setting('center_lng'),
                'zoom' => get_setting('zoom'),
                'height' => '',
            ),
            /*array (
                'key' => 'field_539517e877f89',
                'label' => 'Map Icon',
                'name' => 'map_icon',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),*/
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    );
    
    add_action('plugins_loaded',function() use ($group){
    
        register_field_group(attach_to_post_types($group));
    
    });
}

/**
 * Dynamically attach these fields to post types defined returned by 
 * get_post_types() in the admin
 *
 * @see get_post_types;
 */
function attach_to_post_types($group) {

    $cntr = 0;
    $post_types = get_post_types();
    $group['location'] = [];

    foreach($post_types as $type){

        $group['location'][] = [[
            'param' => 'post_type',
            'operator' => '==',
            'value' => $type,
            'order_no' => $cntr,
            'group_no' => 0,
            ]];

        $cntr ++;
    };

    return $group;
}

/**
 * Return array of post type names that should have these fields added
 */
function get_post_types() {

    //Pulled from the admin, see admin.php
    $types = WordPress::get_option(SETTING_NS,'post_types') ?: 'post';

    return apply_filters('tf-google-map-post-types', array_map('trim',explode(',',$types)));
}
