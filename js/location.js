theFoldGoogleMap.prototype = _.extend(theFoldGoogleMap.prototype, {

    locationControl: function(controlDiv, map) {

        controlDiv.className = "gmnoprint custom-control-container";

        var controlUIContainer = document.createElement( 'div' ),
            controlUI = document.createElement( 'div' );

        controlUIContainer.className = "gm-style-mtc";
        controlDiv.appendChild( controlUIContainer );

        controlUI.className = "custom-control";
        controlUI.title = 'Click to center map at your location';
        controlUI.innerHTML = 'My Location &nbsp;<i class="icon-compass"></i>';
        controlUIContainer.appendChild( controlUI );

        var _this = this;
        
        google.maps.event.addDomListener(controlUI, 'click', function() {
            
            navigator.geolocation.getCurrentPosition(function(position){

                var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                map.setCenter(initialLocation);
                if(map.getZoom() < 16) map.setZoom(16);
            });
        });
    },

    initLocationButton:  function(ready){
    
        var _this = this;

        google.maps.event.addDomListener(window, 'load', function(){

            var div = document.createElement('div');
            div.index = 1;
            var button = new _this.locationControl(div, _this.map);

            _this.map.controls[google.maps.ControlPosition.TOP_RIGHT].push( div );
            
            if(typeof ready === 'function'){
                ready(div);
            }
        });
    }
});
