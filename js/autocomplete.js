theFoldGoogleMap.prototype = _.extend(theFoldGoogleMap.prototype, {

    autocompleteListeners: {},

    autocomplete: function(input, addressSetCallback) {

        this.$input = input;

        if(typeof addressSetCallback === 'function'){
            this.addressCompleted = addressSetCallback;
        }

        this.autocomplete = new google.maps.places.Autocomplete( this.$input[0] );
        this.autocomplete.map = this.map;
        this.autocomplete.bindTo('bounds', this.map);

        // add dummy marker
        this.markers[0] = new google.maps.Marker({
            draggable	: true,
            raiseOnDrag	: true,
            map	: this.map,
        });

        this.addAutocompleteListeners();
    },

    addAutocompleteListeners: function() {
        // events

        var _this = this;

        if( !this.geocoder ) {
            this.geocoder = new google.maps.Geocoder();
        }

        this.autocompleteListeners.place_changed = google.maps.event.addListener(this.autocomplete, 'place_changed', function( e ) {

            var place = this.getPlace(), 
            address = _this.$input.val();

        _this.addressCompleted(address, place);

        // validate
        if( place.geometry ) {

            var lat = place.geometry.location.lat(), 
            lng = place.geometry.location.lng();

        _this.addMarker(lat,lng,0).centerMap();
        }
        else
        {
            // client hit enter, manulaly get the place
            _this.geocoder.geocode({ 'address' : address }, function( results, status ){

                // validate
                if( status != google.maps.GeocoderStatus.OK ){
                    console.log('geocoder failed due to: ' + status);
                    return;
                }

                if( !results[0] ){
                    console.log('no results found');
                    return;
                }

                // get place
                place = results[0];

                var lat = place.geometry.location.lat(),
                lng = place.geometry.location.lng();

            _this.addMarker(lat,lng,0).centerMap();
            });
        }

        });

        this.autocompleteListeners.dragend = google.maps.event.addListener( this.markers[0], 'dragend', function(){

            // vars
            var position = _this.getMarker(0).getPosition(),
            lat = position.lat(),
            lng = position.lng();

        _this.addMarker(lat,lng,0).autocompleteSync();

        });


        this.autocompleteListeners.click = google.maps.event.addListener( this.map, 'click', function( e ) {

            // vars
            var lat = e.latLng.lat(),
            lng = e.latLng.lng();

        _this.addMarker(lat,lng,0).autocompleteSync();

        });
    },

    clearAutocompleteListeners: function() {

        var listener;

        for( listener in this.autocompleteListeners){
            google.maps.event.removeListener(this.autocompleteListeners[listener]);
        }
    },

    autocompleteSync: function() {

        var position = this.getMarker(0).getPosition(),
        latlng = new google.maps.LatLng( position.lat(), position.lng() )
            _this = this;

        this.geocoder.geocode({ 'latLng' : latlng }, function( results, status ){

            // validate
            if( status != google.maps.GeocoderStatus.OK ) {
                console.log('Geocoder failed due to: ' + status);
                return;
            }

            if( !results[0] ){
                console.log('No results found');
                return;
            }

            // get location
            var location = results[0];

            _this.$input.val(location.formatted_address);

            _this.addressCompleted(location.formatted_address, location);

        });

        // return for chaining
        return this;
    },

    locate: function() {

        // reference
        var _this	= this;

        // Try HTML5 geolocation
        if( ! navigator.geolocation )
        {
            alert( 'Sorry your browser doesn\'t support this feature' );
            return this;
        }

        navigator.geolocation.getCurrentPosition(function(position){

            // vars
            var lat = position.coords.latitude,
            lng = position.coords.longitude;

        _this.addMarker(lat,lng,0).autocompleteSync().centerMap();
        });
    },

    clear:function() {

        this.$input.val('');
        // hide marker
        this.getMarker(0).setVisible( false );
    }

});
