function theFoldGoogleMap(config)
{
    config = config || {};

    this.map = null;
    this.$el = null;
    this.directionsService = null;
    this.directionsDisplay = null;
    this.markerClusterer = null;
    this.centeringMap = false;
    this.centeringMapTimeOut = null;
    this.markers = {};
    this.config = config || {};
    this.isLoaded = false;
    this.infoWindow = null;
    $ = jQuery;
    
    /*
     *  render_map
     *
     *  This function will render a Google Map onto the selected jQuery element
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$el (jQuery element)
     *  @return	n/a
     */

    this.renderMap = function( $el ) {

        var $markers = $el.find('.marker'),
            args = jQuery.extend({
                zoom	: parseInt( config.zoom || TheFoldGoogleMapConfig.zoom ),
                center	: new google.maps.LatLng( config.lat || TheFoldGoogleMapConfig.lat, config.lng || TheFoldGoogleMapConfig.lng ),
                mapTypeId : google.maps.MapTypeId.ROADMAP
            },this.config),
            _this = this;

        this.map = new google.maps.Map( $el.find('.canvas')[0], args);
        this.infoWindow = new google.maps.InfoWindow();
        
        if(config.cluster){
            this.markerClusterer = new MarkerClusterer(this.map);
        }
        
        google.maps.event.addListenerOnce(this.map, 'tilesloaded', function(){
            _this.isLoaded = true;
        });

        this.$el = $el;

        // add markers
        $markers.each(function(){

            _this.addMarker(
                $(this).data('lat'),
                $(this).data('lng'),
                $(this).data('post-id'), 
                $(this).html() 
            );
        });

        if(config.cluster){
            this.markerClusterer.repaint();
        }

        if(this.markers.length){
            this.centerMap();
        }
    };

    /*
     *  add_marker
     *
     *  This function will add a marker to the selected Google Map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$marker (jQuery element)
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    this.addMarker = function( lat, lng, postId, html, attrs, listners ) {

        var _this = this,
            latlng = new google.maps.LatLng( lat, lng ),
            attrs = attrs || {},
            listeners = listners || [];

        if(typeof this.markers[postId] === 'undefined'){

            attrs.position = latlng;
            delete attrs.html;

            // create marker
            this.markers[postId] = new google.maps.Marker(attrs);

            if(this.markerClusterer){
                this.markerClusterer.addMarker(this.markers[postId], false); 
            } else {
                this.markers[postId].setMap(this.map);
            }

            // if marker contains HTML, add it to an infoWindow
            if( html )
            {
                // show info window when marker is clicked
                google.maps.event.addListener(this.markers[postId], 'click', function() {

                    _this.infoWindow.close();
                    _this.infoWindow.setContent(html);
                    _this.infoWindow.open( _this.map, this);

                });
            }

            for( var i=0, len = listners.length; i<len ; i++ ){
                google.maps.event.addListener(this.markers[postId], 'click', listeners[i]);
            }
        }
        else {
            //update marker
            this.markers[postId].setPosition(latlng);
            this.markers[postId].setVisible(true);
        }

        // return this for chaining
        return this.markers[postId];
    };
    
    this.deleteMarker = function(postId) {

        if(this.markers[postId] !== undefined) {
        
            if(this.markerClusterer){
                this.markerClusterer.removeMarker(this.markers[postId], false)
            }

            this.markers[postId].setMap(null);

            delete this.markers[postId];
        }
    };
    
    this.getMarker = function(postId){

        return this.markers[postId];
    };
    
    /*
     *  center_map
     *
     *  This function will center the map, showing all markers attached to this map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	map (Google Map object)
     *  @return	n/a
     */
    this.centerMap = function () {

        var _this = this;

        _this.centeringMap = true;

        if(_this.centeringMapTimeOut){
            clearTimeout(_this.centeringMapTimeOut);
        };

        _this.centeringMapTimeOut = setTimeout(function(){
            _this.centeringMap = false;
        },3000);
        
        // vars
        var bounds = new google.maps.LatLngBounds();
        var count = 0;

        // loop through all markers and create bounds
        jQuery.each( this.markers, function( i, marker ){

            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

            bounds.extend( latlng );

            count += 1;

        });

        if( count == 0){

            var latlng = new google.maps.LatLng( this.$el.find('.input-lat').val(), this.$el.find('.input-lng').val() );
            bounds.extend( latlng );
        }

        if( count <= 1 )
        {
            // set center of map
            this.map.setCenter( bounds.getCenter() );
            this.map.setZoom( 17 );
        }
        else
        {
            // fit to bounds
            this.map.fitBounds( bounds );
        }
        
        // return this for chaining
        return this;
    };

    this.resize = function() {
        google.maps.event.trigger(this.map, 'resize');
    }
    
    this.showMarkers = function() {
        this.setAllMap(this.map);
    }

    this.clearMarkers = function() {

        this.setAllMap(null)
    };

    this.deleteMarkers = function() {

        this.clearMarkers();
        this.markers = {};
    };

    this.deleteNotIn = function(markers){

        var existing_ids = _.keys(this.markers),
            new_ids = markers.map(function(marker){
                return marker.post_id;
            }), 
            // convert keys in above arrays to ints so we compare correctly
            to_delete = _.difference(existing_ids, new_ids);

        for (var i=0, len = to_delete.length; i<len; i++) {
            this.deleteMarker(to_delete[i]);
        }
    }

    this.setAllMap = function(map) {

        jQuery.each( this.markers, function( i, marker ){

            marker.setMap(map)
        });
    };

    this.clearDirections = function(showMarkers){

        var showMarkers = typeof showMarkers !== 'undefined' ? showMarkers : true;

        if(this.directionsDisplay){
            this.directionsDisplay.setMap(null);
            this.directionsDisplay.setPanel(null);
        }

        if(showMarkers){
            this.showMarkers()
        }
    };

    this.calcRoute = function(posts, optimize) {

        var batches = [], stops = [], itemsPerBatch = 10, itemsCounter = 0;
        var optimize = typeof optimize !== 'undefined' ? optimize : false;

        for(var i=0; i < posts.length; i++){
            stops.push(this.markers[posts[i].post_id]);
        }
        
        var wayptsExist = stops.length > 0;

        if(!this.directionsService){
            this.directionsService = new google.maps.DirectionsService();
        }

        this.clearDirections(false);
        this.clearMarkers();

        this.directionsDisplay = new google.maps.DirectionsRenderer({
            map: this.map
        });

        if(jQuery('#js-directions-panel').length > 0){

            this.directionsDisplay.setPanel(jQuery('#js-directions-panel')[0]);
        }

        while(wayptsExist){

            var subBatch = [];
            var subitemsCounter = [];

            for (var j = itemsCounter; j < stops.length; j++) {
                subitemsCounter++;
                subBatch.push({
                    location: stops[j].address ? stops[j].address : stops[j].getPosition(),
                    stopover: true
                });
                if (subitemsCounter == itemsPerBatch)
                    break;
            }

            itemsCounter += subitemsCounter;
            batches.push(subBatch);
            wayptsExist = itemsCounter < stops.length;
            itemsCounter--;
        }
        
        // now we should have a 2 dimensional array with a list of a list of waypoints
        var combinedResults;
        var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
        var directionsResultsReturned = 0;
        var _this = this;

        for (var k = 0; k < batches.length; k++) {

                var lastIndex = batches[k].length - 1;
                var start = batches[k][0].location;
                var end = batches[k][lastIndex].location;

                // trim first and last entry from array
                var waypts = [];
                waypts = batches[k];
                waypts.splice(0, 1);
                waypts.splice(waypts.length - 1, 1);

                var request = {
                    origin: start,
                    destination: end,
                    waypoints: waypts,
                    optimizeWaypoints: optimize,
                    travelMode: google.maps.TravelMode.DRIVING
                };

                (function (kk) {
                    _this.directionsService.route(request, function (result, status) {
                        if (status == window.google.maps.DirectionsStatus.OK) {

                            var unsortedResult = { order: kk, result: result };
                            unsortedResults.push(unsortedResult);

                            directionsResultsReturned++;

                            if (directionsResultsReturned == batches.length) { // we've received all the results. put to map

                                // sort the returned values into their correct order
                                unsortedResults.sort(function (a, b) { return parseFloat(a.order) - parseFloat(b.order); });

                                var count = 0;
                                for (var key in unsortedResults) {
                                    if (unsortedResults[key].result != null) {
                                        if (unsortedResults.hasOwnProperty(key)) {
                                            if (count == 0) { // first results. new up the combinedResults object
                                                combinedResults = unsortedResults[key].result;
                                            } else {
                                                // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete 
                                                // directionResults object, but enough to draw a path on the map, which is all I need
                                                combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);

                                                combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                            }
                                            count++;
                                        }
                                    }
                                }
                                _this.directionsDisplay.setDirections(combinedResults);
                            }
                        }
                    });
                })(k);
        }
    };

};

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
 
jQuery(document).ready(function($){

    var maps = [];

    $('.acf-map').each(function(i){

        var maps[i] = new theFoldGoogleMap();

        maps[i].renderMap( $(this) );
    });
});
*/
