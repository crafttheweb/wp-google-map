<?php
namespace TheFold\Locations;

function js_dir($path='') {
    return plugins_url('js/'.ltrim($path,'/'), __FILE__ );
}

function css_dir($path='') {
    return plugins_url('css/'.ltrim($path,'/'), __FILE__ );
}

add_action('wp_enqueue_scripts',function(){

    $region = null;
    if($r = get_setting('region')){
        $region = '&region='.$r;
    }

    wp_enqueue_style('acf-map',css_dir('acf-map.css'),[],1,'all');
    wp_enqueue_script('google-maps','//maps.googleapis.com/maps/api/js?v=3&libraries=places'.$region,[],'3',true);
    wp_enqueue_script('acf-map',js_dir('acf-map.js'),['jquery','google-maps'],'1.3',true);

    wp_localize_script('acf-map','TheFoldGoogleMapConfig',[
        'lat' => get_setting('center_lat'), 
        'lng' => get_setting('center_lng'),
        'zoom' => get_setting('zoom')
    ]);

    wp_register_script('map-autocomplete',js_dir('autocomplete.js'),['acf-map','underscore'],'1',true);
    wp_register_script('map-location',js_dir('location.js'),['acf-map','underscore'],'1',true);

});
