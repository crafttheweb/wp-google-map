<?php

namespace TheFold\Locations;

use \TheFold\WordPress\Setting;
use \TheFold\WordPress;

$settings = new Setting\Section(SETTING_NS,'Settings', null, 'thefold-locations');

new Setting\Page(SETTING_NS,'Location Options',array(

    new Setting\Field( 'post_types','Post Types ( comma seperated )', $settings ),
    new Setting\Field( 'center_lat','Default Latitude', $settings ),
    new Setting\Field( 'center_lng','Default Longitude', $settings ),
    new Setting\Field( 'zoom','Default Zoom', $settings ),
    new Setting\Field( 'region','Region', $settings ),
    //new Setting\Field( 'maker_url','Marker Image', $settings ),
));

function get_setting($name){

    $defaults = [
        'center_lat' => apply_filters('tf-google-map-lat', -36.8404),
        'center_lng' => apply_filters('tf-google-map-lng', 174.7399),
        'zoom' => apply_filters('tf-google-map-zoom', 12),
    ];

    return WordPress::get_option(SETTING_NS, $name, isset($defaults[$name]) ? $defaults[$name] : null );
}
