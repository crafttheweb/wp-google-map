<?php 
global $post;
$post = $location;
setup_postdata( $post );

$field = get_field('location');
?>
<div class="marker" data-post-id="<?=$post->ID?>" data-lat="<?= $field['lat']; ?>" data-lng="<?= $field['lng']; ?>">
    <h4><?php the_title(); ?></h4>
    <p class="address"><?php echo $field['address']; ?></p>
    <?php the_content(); ?>
</div>

<?php wp_reset_postdata(); ?>
