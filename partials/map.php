<?php
namespace TheFold\Locations;
?>
<div <?= !empty($map_id) ? "id='$map_id'" : ''; ?> class="acf-map">
    <div class="canvas">
    </div>
    <div style="display:none;">
        <?php 
        foreach ( $locations as $location ) :
            marker($location); 
        endforeach; 
        ?>
        <input type="hidden" class="input-lat" value="<?= get_setting('center_lat') ?>" />
        <input type="hidden" class="input-lng" value="<?= get_setting('center_lng') ?>" />
    </div>
</div>
