<?php
/**
 * Plugin Name: The Fold Google Map Locations
 * Author: The Fold
 * Author URI: http://www.thefold.co.nz
 */

namespace TheFold\Locations;

require_once ABSPATH.'/../../vendor/autoload.php';

const SETTING_NS = 'thefold-locations';

require __DIR__.'/admin.php';
require __DIR__.'/acf.php';
require __DIR__.'/js.php';
require __DIR__.'/partials.php';
