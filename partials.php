<?php

namespace TheFold\Locations;

use TheFold\WordPress;

function partials_dir($path='') {
    return __DIR__.'/partials/'.ltrim($path,'/');
}

function map($locations, $slug='partials/map', $view_params=[]) {

    $view_params = array_merge(['locations'=>$locations],$view_params);

    WordPress::render_template([
        'slug' => $slug,
        'view_params' => $view_params, 
        'default_path' => partials_dir('map')
    ]);
}

function marker($location, $slug='partials/marker', $view_params=[]) {

    $view_params = array_merge(['location'=>$location],$view_params);

    WordPress::render_template([
        'slug' => $slug,
        'view_params' => $view_params, 
        'default_path' => partials_dir('marker')
    ]);
}
